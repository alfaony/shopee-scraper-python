from selenium import webdriver
from bs4 import BeautifulSoup
import re
import csv
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd
import time

chrome_options = Options()
# chrome_options.add_argument('--headless')
chrome_options.add_argument('log-level=2')
driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
katakunci = input('Masukkan kata kunci : ')

def search(katakunci):
    links = []
    products=[]
    prices=[]
    print('mencari semua product dengan kata kunci ' + katakunci)
    url = 'https://shopee.co.id/search?facet=11043565&keyword=' + katakunci +'&noCorrection=true&page=0'
    try:
        driver.get(url)
        time.sleep(60)
        driver.execute_script('window.scrollTo(0, 1500);')
        time.sleep(25)
        driver.execute_script('window.scrollTo(0, 2500);')
        time.sleep(25)
        soup_a = BeautifulSoup(driver.page_source, 'html.parser')
        coba = soup_a.find_all('div', {'class': 'shopee-search-item-result__item'})
        

        for item in soup_a.find_all('div', {'class': 'shopee-search-item-result__item'}):
#             print(item)
            name = item.find('div', {'class': '_10Wbs- _2STCsK _3IqNCf'}) 
            if name is not None:
                name = name.text.strip()
            else:
                name = ''
     
            price = item.find('span', {'class': '_3c5u7X'})
            if price is not None:
                price = 'Rp '+price.text.strip()
            else:
                price = 'Rp'
            print(name+" - "+price)
            
            products.append(name)
            prices.append(price) 

        df = pd.DataFrame({'Product Name':products,'Price':prices}) 
        df.to_csv('products.csv', index=False, encoding='utf-8')
#         products = soup_a.find('div',{'class': 'col-xs-2-4 shopee-search-item-result__item'})
#         print(soup_a)
        
#         for link in products.find_all('a'):
#             links.append(link.get('href'))
#             print(link.get('href'))
    except TimeoutException:
        print('failed to get links with query ' + line)
    return links

product_urls = search(katakunci)
